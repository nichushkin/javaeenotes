//package it.hillel.webapp;
//
//import it.hillel.webapp.exception.ApplicationException;
//import it.hillel.webapp.rest.service.LoginService;
//
//import javax.servlet.*;
//import javax.servlet.http.*;
//import javax.servlet.annotation.*;
//import java.io.IOException;
//
//@WebServlet(name = "LoginServlet", value = "/LoginServlet")
//public class LoginServlet extends HttpServlet {
//
//    @Override
//    public void init() throws ServletException{
//        super.init();
//    }
//
//    @Override
//    public void destroy(){
//        super.destroy();
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//        request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String login = request.getParameter("login");
//        String password = request.getParameter("password");
//
//        System.out.println(login);
//        System.out.println(password);
//
//        if(login.equals("admin") && password.equals("qwerty")){
//            response.sendRedirect(getServletContext().getContextPath() + "/dashboard");
//        } else {
//            request.setAttribute("error", "true");
//            request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
//        }
//
//    }
//
//    public static class LoginServlet extends HttpServlet {
//
//        private final LoginService loginService = new LoginService();
//
//        @Override
//        public void init() throws ServletException {
//            super.init();
//        }
//
//        @Override
//        public void destroy() {
//            super.destroy();
//        }
//
//        @Override
//        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//            HttpSession session = request.getSession();
//
//            // 1. Запрашиваем страницу в браузере: переходим по адресу http://localhost:8080/helloapp/login
//            request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
//
//        }
//
//        @Override
//        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//            // 3. Обрабатываем запрос со страницы login тут. Достаем параметры формы и делаем проверку введенных данных.
//
//            String login = request.getParameter("login");
//            String password = request.getParameter("password");
//
//            try {
//
//                // 4. Проверяем логин и пароль
//
//                this.loginService.login(login, password);
//
//
//                response.sendRedirect(getServletContext().getContextPath() + "/dashboard");
//
//            } catch (ApplicationException e) {
//                // 4.2. Если возникла ошибка возвращаем пользователя на страницу login и показываем ошибку
//                request.setAttribute("error", "true");
//                request.setAttribute("errorMsg", e.getMessage());
//                request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
//            }
//
//        }
//
//    }
//}
