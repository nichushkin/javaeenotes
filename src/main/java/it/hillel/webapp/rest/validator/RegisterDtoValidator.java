package it.hillel.webapp.rest.validator;

import it.hillel.webapp.exception.ApplicationException;
import it.hillel.webapp.rest.dto.RegisterDto;

public class RegisterDtoValidator implements Validator<RegisterDto> {
    @Override
    public void validate(RegisterDto dto) {
        if(dto.getLogin() == null || dto.getLogin().isEmpty()){
            throw new ApplicationException("Input login");
        }

        if (dto.getPassword() == null || dto.getPassword().isEmpty()){
            throw new ApplicationException("Input password");
        }

        if (dto.getConfirmPassword() == null || dto.getConfirmPassword().isEmpty()){
            throw new ApplicationException("Input confirm password");
        }

        if (!dto.getPassword().equals(dto.getConfirmPassword())){
            throw new ApplicationException("passwords are different");
        }
    }
}
