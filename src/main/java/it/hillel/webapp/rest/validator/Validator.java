package it.hillel.webapp.rest.validator;

public interface Validator<T> {
    void validate(T dto);
}
