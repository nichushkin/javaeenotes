package it.hillel.webapp.rest.model;

public class UserDto {

    private String login;
    private String lastName;
    private String firstName;

    public UserDto(String login, String lastName, String firstName) {
        this.login = login;
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
