package it.hillel.webapp.rest.component;

import it.hillel.webapp.rest.entity.UserEntity;

public class SecurityContext {

    private static final ThreadLocal<UserEntity> userThread = new ThreadLocal<>();

    public static UserEntity get(){
        return userThread.get();
    }

    public static void set(UserEntity user){
        userThread.set(user);
    }
}
