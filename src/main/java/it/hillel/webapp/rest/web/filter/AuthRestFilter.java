package it.hillel.webapp.rest.web.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import it.hillel.webapp.rest.component.SecurityContext;
import it.hillel.webapp.rest.dto.ResponseDto;
import it.hillel.webapp.rest.model.UserDto;
import it.hillel.webapp.rest.service.UserService;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@WebFilter(filterName = "AuthFilter", urlPatterns = "/*")
public class AuthRestFilter implements Filter {
    private final UserService userService = new UserService();
    private JwtParser jwtParser;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final List<String> PROTECTED_PATHS = new ArrayList<>();

    public void init(FilterConfig config) throws ServletException {

        this.jwtParser = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes(StandardCharsets.UTF_8)))
                .build();
        PROTECTED_PATHS.add("/api/v1/user");
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String requestUri = req.getRequestURI().replace(req.getContextPath(), "");


        if (PROTECTED_PATHS.stream().anyMatch(url -> url.startsWith(requestUri))) {
            String authHeader = req.getHeader("Authorization");
            if (authHeader == null || authHeader.isEmpty()) {
                resp.setStatus(401);
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto("Where is token")));
                resp.getWriter().flush();

                return;
            }

            String jwtToken = authHeader.replace("Bearer", "");

            Claims claims = this.jwtParser.parseClaimsJws(jwtToken).getBody();
            if (!claims.getSubject().equals("admin")) {
                resp.setStatus(401);
                response.setContentType("application/json");
                response.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto("Token is wrong")));
                response.getWriter().flush();

                return;
            }

            SecurityContext.set(this.userService.findByLogin("admin"));

            chain.doFilter(request, response);

        } else {
            chain.doFilter(request, response);
        }


    }
}
