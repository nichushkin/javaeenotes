package it.hillel.webapp.rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.hillel.webapp.rest.component.SecurityContext;
import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.rest.model.TokenDto;
import it.hillel.webapp.rest.model.UserDto;
import it.hillel.webapp.rest.service.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "UserRestServlet", urlPatterns = "/api/v1/user")
public class UserRestServlet extends HttpServlet {

    private final ObjectMapper objectMapper = new ObjectMapper();
//    private final UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        UserEntity user = this.userService.findByLogin("admin");

        UserEntity user = SecurityContext.get();

        response.setStatus(200);
        response.setContentType("application/json");
        response.getWriter().write(this.objectMapper.writeValueAsString(new UserDto(user.getLogin(), user.getLastName(), user.getFirstName())));
        response.getWriter().flush();
    }

}
