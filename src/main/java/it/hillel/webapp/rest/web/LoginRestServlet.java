package it.hillel.webapp.rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import it.hillel.webapp.exception.ApplicationException;
import it.hillel.webapp.rest.dto.LoginDto;
import it.hillel.webapp.rest.dto.ResponseDto;
import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.rest.model.TokenDto;
import it.hillel.webapp.rest.service.LoginService;

import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.UUID;

@WebServlet(urlPatterns = {"/api/v1/auth/login"}, loadOnStartup = 1)
public class LoginRestServlet extends HttpServlet {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final LoginService loginService = new LoginService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDto dto = this.objectMapper.readValue(request.getInputStream(), LoginDto.class);

        try {
            Date now = new Date();
            UserEntity user = this.loginService.login(dto.getLogin(), dto.getPassword());

            String token = Jwts.builder()
                    .setSubject(user.getLogin())
                    .setExpiration(new Date(now.getTime() + 1800000))
                    .setIssuedAt(now)
                    .setId(UUID.randomUUID().toString())
                    .signWith(Keys.hmacShaKeyFor("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes(StandardCharsets.UTF_8)), SignatureAlgorithm.HS256)
                    .compact();
            response.setStatus(200);
            response.setContentType("application/json");
            response.getWriter().write(this.objectMapper.writeValueAsString(new TokenDto(token)));
            response.getWriter().flush();

        } catch (ApplicationException e) {
            response.setStatus(400);
            response.setContentType("application/json");
            response.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
            response.getWriter().flush();
        }

    }
}
//public class LoginRestServlet extends ServletBase<LoginDto> {
//    private final LoginService loginService = new LoginService();
//
//    public LoginRestServlet(Class<?> requestDtoClass) {
//        super(requestDtoClass);
//    }
//
//    @Override
//    protected void processPost(LoginDto dto) {
//        Date now = new Date();
//        this.loginService.login(dto.getLogin(), dto.getPassword());
//        дальше может быть ошибка, т.к. не совсем понимаю физику работы токена
//        String token = Jwts.builder()
//                    .setSubject(user.getLogin())
//                    .setExpiration(new Date(now.getTime() + 1800000))
//                    .setIssuedAt(now)
//                    .setId(UUID.randomUUID().toString())
//                    .signWith(Keys.hmacShaKeyFor("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes(StandardCharsets.UTF_8)), SignatureAlgorithm.HS256)
//                    .compact();
//            response.setStatus(200);
//            response.setContentType("application/json");
//            response.getWriter().write(this.objectMapper.writeValueAsString(new TokenDto(token)));
//            response.getWriter().flush();
//    }
//}
