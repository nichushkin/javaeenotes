package it.hillel.webapp.rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.hillel.webapp.exception.ApplicationException;
import it.hillel.webapp.rest.dto.LoginDto;
import it.hillel.webapp.rest.dto.RegisterDto;
import it.hillel.webapp.rest.dto.ResponseDto;
import it.hillel.webapp.rest.service.LoginService;
import it.hillel.webapp.rest.validator.RegisterDtoValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegisterRestServlet extends HttpServlet {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final LoginService loginService = new LoginService();

    private final RegisterDtoValidator registerDtoValidator = new RegisterDtoValidator();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RegisterDto dto = this.objectMapper.readValue(request.getInputStream(), RegisterDto.class);

        try {

            this.registerDtoValidator.validate(dto);
            this.loginService.register(dto.getFirstName(), dto.getLastName(), dto.getLogin(), dto.getPassword());
            response.setStatus(200);

        } catch (ApplicationException e) {
            response.setStatus(400);
            response.setContentType("application/json");
            response.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
            response.getWriter().flush();
        }

    }
}
//public class RegisterRestServlet extends ServletBase<RegisterDto> {
//    private final LoginService loginService = new LoginService();
//    private final RegisterDtoValidator registerDtoValidator = new RegisterDtoValidator();
//
//    public RegisterRestServlet() {
//        super(RegisterDto.class);
//    }
//
//    @Override
//    protected void processPost(RegisterDto dto) {
//        this.registerDtoValidator.validate(dto);
//        this.loginService.register(dto.getFirstName(), dto.getLastName(), dto.getLogin(), dto.getPassword());
//    }
//}

