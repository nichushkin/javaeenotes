package it.hillel.webapp.rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.hillel.webapp.exception.ApplicationException;
import it.hillel.webapp.rest.dto.ResponseDto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class ServletBase<T> extends HttpServlet {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Class<?> requestDtoClass;

    public ServletBase(Class<?> requestDtoClass) {
        this.requestDtoClass = requestDtoClass;
    }

    protected abstract void processPost(T requestDto);

    @Override
    protected void doPost (HttpServletRequest req, HttpServletResponse resp) throws IOException {
        T requestDto = (T) this.objectMapper.readValue(req.getInputStream(), requestDtoClass);
        try {
            processPost(requestDto);
        } catch (ApplicationException e){
            resp.setStatus(400);
            resp.setContentType("application/json");
            resp.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
            resp.getWriter().flush();
        }
    }
}
