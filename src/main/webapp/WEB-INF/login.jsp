<%--
  Created by IntelliJ IDEA.
  User: My
  Date: 16.08.2022
  Time: 15:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<form action="/helloapp/login" method="post">
  <%if("true".equals(request.getAttribute("error"))) {%>
  <h5 style="color: red">Login or password wrong</h5>
  <%}%>

  <input type="text" name="login" />
  <input type="password" name="password" />
  <button type="submit">
    Login
  </button>
</form>
</body>
</html>
